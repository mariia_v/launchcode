const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');

const connection = mysql.createPool({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'launchcodedb'
});

/* Start the app.*/
const app = express();

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
  });
  /**  Creat a GET route that will return data from the 'quotes' table.*/
app.get('/quotesget', function (req, res) {
    // Connect to the database.
    connection.getConnection(function (err, connection) {

    // Execut the MySQL query .
    connection.query('SELECT * FROM quotes', function (error, results, fields) {
      if (error) throw error;
      // Get the 'response' from db and sent it to route.
      res.send(JSON.stringify(results))
    });
  });
});

// Start our server.
app.listen(3000, () => {
 console.log('http://localhost:3000/quotesget Our data is here.');
});