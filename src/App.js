import React from 'react';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Header } from './components/Header';
import Sidebar from './components/Sidebar';
import Quotes  from './components/Quotes';

function App() {
  return (
    <React.Fragment>
      <Router>
        <Header />
        <Sidebar />
        <Switch>
  <Route exact path="/quotes" component={Quotes} />
  
</Switch>
      </Router>
</React.Fragment>
  );
}

export default App;
