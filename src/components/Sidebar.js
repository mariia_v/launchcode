import { render } from "@testing-library/react";

import React from 'react';
import styled from 'styled-components';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
 

/* Styles for our side bar that will apper on the page */
const StyledSideNav = styled.div`
  position: fixed;     /* Fixed Sidebar (stay in place on scroll and position relative to viewport) */
  height: 100%;
  width: 14%;     /* Set the width of the sidebar */
  z-index: 1;      /* Stay on top of everything */
  top: 3.4em;      /* Stay at the top */
  background-color: #aab1d4; 
  overflow-x: hidden;     /* Disable horizontal scroll */
  padding-top: 30px;
  margin-top:12px;
  border-radius: 3px;
`;
const NavIcon = styled.div`
`;
/**Styles for each component on the side bar */
const StyledSidebarComponent = styled.div`
height: 70px;
margin-left: 11%;

width: 14%; /* width must be same size as NavBar to center */
text-align: center; /* Aligns <a> inside of NavIcon div */
margin-bottom: 0;   /* Puts space between NavItems */
a {
    font-size: 1.3em;
    
    color: ${(props) => props.active ? "#E7F0C3" : "white"};
    :hover {
    opacity: 0.7;
    color: white;
    text-decoration: underline; /* Gets rid of underlining of icons */
    }  
}
`;
/**Class representing the side navigation having the constractor and showing all the components of the side bar */
class SideNav extends React.Component{ 
    constructor(props) {
        super(props);
        this.state = {
          activePath: '/',
          items: [
            {
              path: '', /* path shows where to navigate after clicking */
              name: 'Home', /**Name appear next to icon */
              css: 'fa fa-lg fa-home', /**font awesome style */
              key: 1 
            },
            {
                path: '/quotes',
                name: 'Quotes',
                css: 'fa fa-lg fa-clock',
                key: 2 
              },
            {
              path: '',
              name: 'Leads',
              css: 'fa fa-lg fa-book',
              key: 3
            },
            {
              path: '',
              name: 'Tours',
              css: 'fa fa-paper-plane',
              key: 4
            },
            {
                path: '',
                name: 'Invoices',
                css: 'fa fa-file fa-lg',
                key: 5
              },
              {
                path: '',
                name: 'Analytics',
                css: 'fa fa-cog fa-lg',
                key: 6
              },
              {
                path: '',
                name: 'Team',
                css: 'fa fa-users fa-lg',
                key: 7
              },
              {
                path: '',
                name: 'Admin',
                css: 'fa fa-cog fa-lg',
                key: 8
              }
          ]
        }  
      }
      onItemClick = (path) => {
        /* css will be change when path is activePath */
        this.setState({ activePath: path }); 
      }
    render() {
        const { items, activePath } = this.state;
        return (
          <StyledSideNav>
            {
              items.map((item) => {
                return (
                  <SidebarComponent path={item.path} name={item.name} css={item.css} 
                  onItemClick={this.onItemClick} 
                  active={item.path === activePath} key={item.key}/>
                )
              })
            }
          </StyledSideNav>
        );
      }}

 /**Class represents the side bar component that handle click and shows the component on the page */       
class SidebarComponent extends React.Component{ 
    handleClick = () => {
        const { path, onItemClick } = this.props;
        onItemClick(path);
      }
    render() {
        const { active } = this.props;
        return(
            <StyledSidebarComponent active={active}>
                <Link to={this.props.path} className={this.props.css} onClick={this.handleClick}>
                {this.props.name} <NavIcon></NavIcon>
                </Link>
                </StyledSidebarComponent>
    );}}        
    


export default class Sidebar extends  React.Component{ 
    render() {
        return(
                <SideNav></SideNav>
    );}}