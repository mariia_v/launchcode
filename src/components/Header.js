import React from 'react';
import { Nav, Navbar, Form, FormControl } from 'react-bootstrap';
import styled from 'styled-components';
import wetbat from './wetbat.png';
//styles for our header bar
const Styles = styled.div`
  .navbar { background-color: #5F6CAF; 
    
}
  a, .navbar-nav, .navbar-light .nav-link {
    
    color: #white;
    &:hover { color: white; }
  }
  .navbar-brand {
    font-size: 1.4em;
    color: white;
    &:hover { color: white; }
  }
  .form-center {
    position: absolute !important;
    left: 55%;
    right: 25%;
  }
`;
/**Represents our header nav bar */
export const Header = () => (
    <Styles>
      <Navbar expand="lg">
        <Navbar.Brand href="/"><img src={wetbat} width="120" height="40"/></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
        <Form className="form-center">
          <FormControl type="text" placeholder="Search" className="" />
        </Form>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Item><Nav.Link href="/"><i class="fa fa-bell" aria-hidden="true"></i></Nav.Link></Nav.Item>
            <Nav.Item><Nav.Link href="/"><i class="fa fa-comment" aria-hidden="true"></i></Nav.Link></Nav.Item>
            <Nav.Item><Nav.Link href="/"><i class="fa fa-cog" aria-hidden="true"></i>  </Nav.Link></Nav.Item>
            <Nav.Item><Nav.Link href="/"><i class="fa fa-user" aria-hidden="true"></i></Nav.Link></Nav.Item>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </Styles>
  )