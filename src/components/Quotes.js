import React from 'react';
import './Quotes.css'; 
/**Quotes class representing the page that shows list of quotes with details */
export default class Quotes extends React.Component {
  //create the constractor for the class
    constructor(props) {
      //use super to override Component class constructor
        super(props) 
        this.state = { 
          //our array of quotes
           quotes: []
        }      
}
/**method that fetch our data and sent it to quotes */
componentDidMount(){
    let self = this;
    fetch('http://localhost:3000/quotesget',{
        method: 'GET'
    }).then(function(response){
        if (response.status >= 400) {
            throw new Error("Bad response from server");
        }
        return response.json();
    })
    .then(function(data) {
        self.setState({quotes: data});
    }).catch(err => {
    console.log('caught it!',err);
    })
}
       //rendering and returning the table of quotes with information 
     render() { 
        return (
           <div>
              <h1>List of quotes</h1>
              <table id='quotes'>
              
            <thead>
              <tr>
                <th>ID</th>
                <th>Departure Location</th>
                <th>Destination Location</th>
                <th>Departure Date</th>
                <th>Return Date</th>
                <th>Number of Travelers</th>
                <th>Transportation</th>
                <th>Phone Number</th>
              </tr>
            </thead>
              <tbody>
              {this.state.quotes.map(quote => 
            <tr key={quote.quote_id}>
               <td>{quote.quote_id}</td>
               <td>{quote.departure_location}</td>
               <td>{quote.destination_location}</td>
               <td>{quote.departure_date}</td>
               <td>{quote.return_date}</td>
               <td>{quote.number_of_travelers}</td>
               <td>{quote.transportation}</td>
               <td>{quote.phone_number}</td>
            </tr>
     )}
               </tbody>
            </table>
           </div>
        )
     }
   }
