
DROP DATABASE launchcodedb;
CREATE DATABASE IF NOT EXISTS launchcodedb;
USE launchcodedb;

CREATE TABLE quotes (
    quote_id MEDIUMINT NOT NULL AUTO_INCREMENT,
    departure_location VARCHAR(150) NOT NULL,
    destination_location VARCHAR(150) NOT NULL,
    departure_date DATE NOT NULL,
    return_date DATE NOT NULL,
    number_of_travelers INT NOT NULL,
    transportation VARCHAR(150) NOT NULL,
    phone_number VARCHAR(150) NOT NULL,
    PRIMARY KEY (quote_id),
    CHECK (return_date>=departure_date)
);

INSERT INTO quotes (departure_location,destination_location,
departure_date,return_date,number_of_travelers,transportation,phone_number)
VALUES ('YYC','YYZ','2021-01-12','2021-02-01',2,'Bus','+14031234556'
);
INSERT INTO quotes (departure_location,destination_location,
departure_date,return_date,number_of_travelers,transportation,phone_number)
VALUES ('YYC','YVR','2021-01-10','2021-03-01',2,'Taxi','+14031234446'
);
INSERT INTO quotes (departure_location,destination_location,
departure_date,return_date,number_of_travelers,transportation,phone_number)
VALUES ('YYC','YYJ','2021-02-10','2021-05-13',5,'Bus','+14038934446'
);
INSERT INTO quotes (departure_location,destination_location,
departure_date,return_date,number_of_travelers,transportation,phone_number)
VALUES ('YYC','YWG','2021-01-10','2021-03-03',4,'Rent Car','+14031238846'
);

